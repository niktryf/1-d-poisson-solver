/****************************************************
 ***
 *** Poisson 1D Solver
 ***
 ****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265358979323846

void createRho(double *rho, int size);
void jacobiPoisson1D (double *u, double *rho, double h, int size);

int main () {
  /* Declarations */
  int i, j, size;
  double uBoundLeft, uBoundRight, h;
  double *u, *rho;

  //TO DO: make command line arguments later
  size = 51;
  uBoundLeft = 0;
  uBoundRight = 6;
  /* Implied: Length of domain = 1.0 */
  h = 1.0/(size-1);
  printf("\ndx = %f\n", h);

  /***************************************************
                Memory Allocation 
   **************************************************/
  /* Jacobi */
  u = malloc((size)*sizeof(double));
  rho = malloc((size)*sizeof(double));
  
  /* Create Source Term */
  createRho(rho, size);

  /* Initialize u (zeros for now) */
  u[0] = uBoundLeft;
  for(i=1; i<size-1; i++) {
    u[i] = 0;
  }
  u[size-1] = uBoundRight;
  
  /* Call Jacobi */
  jacobiPoisson1D (u, rho, h, size);

  /* Test print solution 
  for (i=0; i<size; i++) {
    printf("x = %f: %f \texact: %f\n", (double)i/(size-1), u[i], 6*((double)i/(size-1))*((double)i/(size-1))*((double)i/(size-1)));
  }*/

  /* Clean Up */
  free(u); free(rho);

  return 0;
}

/* Source Term Function */
void createRho(double *rho, int size) 
{
  int i;
  
  /* Create desired source term rho */
  for (i=0;i<size;i++) {
    rho[i] = 36*(double)i/(size-1);
  }
}
