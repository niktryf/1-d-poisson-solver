/********************************************
 ***
 ***  Jacobi functions
 ***
 ********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define PI 3.14159265358979323846

/* Jacobi Iteration */
void jacobiIteration1D (double *u, double *u_old, double *rho, double h, int size)
{
  int i;

  /* Iterate through all elements */
  for (i=1; i<size-1; i++) {
    u[i] = 0.5*(u_old[i-1] + u_old[i+1] - h*h*rho[i]);
  }
}

/* Jacobi SOR Iteration. w: over-relaxation weight DOES NOT WORK FOR w > 1 !!! */
void jacobiSORIteration1D (double *u, double *u_old, double *rho, double h, double w, int size)
{
  int i;

  /* Iterate through all elements */
  for (i=1; i<size-1; i++) {
    u[i] = w*0.5*(u_old[i-1] + u_old[i+1] - h*h*rho[i]) + (1.0-w)*u_old[i];
  }
}

/* Jacobi Symmetric SOR Iteration. w: over-relaxation weight DOES NOT WORK FOR w > 1 !!! */
void jacobiSSORIteration1D (double *u, double *u_old, double *rho, double h, double w, int size)
{
  int i;

  /* Iterate through all elements forward */
  for (i=1; i<size-1; i++) {
    u[i] = u_old[i] + w*(0.5*(u_old[i-1] + u_old[i+1] - h*h*rho[i]) - u_old[i]);
  }

  for (i=0; i<size; i++) u_old[i] = u[i];

  /* Iterate through all elements backward */
  for (i=size-2; i>=1; i--) {
    u[i] = u_old[i] + w*(0.5*(u_old[i-1] + u_old[i+1] - h*h*rho[i]) - u_old[i]);
  }
}

/* Gauss - Seidel Iteration */
void gaussSeidelIteration1D (double *u, double *rho, double h, int size) 
{
  int i;

  /* Iterate through all elements */
  for (i=1; i<size-1; i++) {
    u[i] = 0.5*(u[i-1] + u[i+1] - h*h*rho[i]);
  }
}

/* Gauss - Seidel Iteration with SOR - w: weight */
void gaussSeidelSORIteration1D (double *u, double *rho, double h, double w, int size) 
{
  int i;

  /* Iterate through all elements */
  for (i=1; i<size-1; i++) {
    u[i] = u[i] + w*(0.5*(u[i-1] + u[i+1] - h*h*rho[i]) - u[i]);
  }
}

/* Gauss - Seidel Iteration with Symmetric SOR - w: weight */
void gaussSeidelSSORIteration1D (double *u, double *rho, double h, double w, int size) 
{
  int i;

  /* Iterate through all elements forward */
  for (i=1; i<size-1; i++) {
    u[i] = u[i] + w*(0.5*(u[i-1] + u[i+1] - h*h*rho[i]) - u[i]);
  }

  /* Iterate through all elements backward */
  for (i=size-2; i>=1; i--) {
    u[i] = u[i] + w*(0.5*(u[i-1] + u[i+1] - h*h*rho[i]) - u[i]);
  }

}

/* Calculates and returns the residual for the 1D poisson
   problem. Uses "quick" jacobi iterative method
   instead of matrix - vector (A * u) multiplication. 
   ----> residual = sqrt ((A*x - r)^2)
 */
double residual (double *u, double *rho, double h, int size)
{
  int i;
  double Ax_r, res; 

  res = 0;
  /* Calculate "A*x - rho" and then add to res. */
  for (i=1; i<size-1; i++) {
    Ax_r = (u[i-1] - 2*u[i] + u[i+1] - h*h*rho[i]);
    res += sqrt(Ax_r*Ax_r);
  }

  return res;
}


/* Jacobi Wrapper Function */
void jacobiPoisson1D (double *u, double *rho, double h, int size) 
{
  int i, j, maxIterations;
  double tolerance, *u_old;
  double start, end;

  /* Set maximum iterations for jacobi solver. 
     We set max = (desired decimal accuracy)/(h^2).
     Since theoretical analysis gives n ~ (2/PI^2)*(d/h^2),
     we set maximum to about 5 times more iterations
  */
  maxIterations = 16.0/(h*h);
  printf("Maximum Iterations: %d\n", maxIterations);
  
  /* Set tolerance (value of residual that is considered 
     adequate). We set tolerance = size*10^(-d).
   */
  tolerance = size * pow(10.0, -13);
  printf("Tolerance = %e\n", tolerance);

  /* Allocate u_old array */
  u_old = malloc((size)*sizeof(double));

  /* Copy u to u_old for first iteration*/
  for(i=0; i<size; i++) u_old[i] = u[i];

  /* Call Jacobi */
  for(i=0; i<maxIterations; i++) {
    //jacobiIteration1D (u, u_old, rho, h, size);
    //jacobiSORIteration1D (u, u_old, rho, h, 1.001, size);
    //jacobiSSORIteration1D (u, u_old, rho, h, 1, size);
    //gaussSeidelIteration1D (u, rho, h, size);
    gaussSeidelSORIteration1D (u, rho, h, 1.89, size); 
    //gaussSeidelSSORIteration1D (u, rho, h, 1.5, size);

    /* Check residual */
    if (residual(u, rho, h, size) <= tolerance) {
      printf("\nResidual: %e\nDesired accuracy reached.\n\n", residual(u, rho, h, size));
      break;
    }
    /* Copy u to u_old */
    for(j=0; j<size; j++) u_old[j] = u[j];
  }

  /* If max iterations reached, give warning */
  if (i==maxIterations) {
    printf("\n*****************************************\n");
    printf("Warning: Maximum Iterations (%d) reached!\n", maxIterations);
    printf("*****************************************\n\n");
  }

  /* Report number of iterations */
  printf("Number of iterations for size N = %d: %d\n\n", size, i);
  printf("Residual: %e\n\n", residual(u, rho, h, size));

  free(u_old);
}
